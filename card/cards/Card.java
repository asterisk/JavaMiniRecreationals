package cards;

abstract public class Card {
    protected Suit suit;
    protected Card(Suit suit) {
        this.suit = suit;
    }

    public String toString() { return suit.toString(); }

}
