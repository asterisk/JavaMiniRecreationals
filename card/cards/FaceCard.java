package cards;

public class FaceCard extends Card{ 
    private Face face;
    public FaceCard(Suit suit, Face face) { 
        super(suit);
        this.face = face;
    }
    public String toString() { return face.toString() + "of" + suit.toString(); }

}
