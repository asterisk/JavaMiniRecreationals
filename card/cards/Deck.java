package cards;
import java.util.LinkedList;

public class Deck{ 
    private LinkedList<Card> cards = new LinkedList<>();
    public boolean IsEmpty(){return cards.size() == 0; }
    public Card draw() { 
        return cards.remove();
    }

    public static Deck makeFrencDeck(){
        Deck deck = new Deck();

        for(Suit s: Suit.values()){
            for(int i=1; i<=10; i++)
                deck.cards.add(new PipCard(s,i));
            for(Face f: Face.values()){
                if(f == Face.CAVALIER) continue;
                deck.cards.add(new FaceCard(s,f)); 
            }
        }
        return deck;
    }
}


