package cards;

class PipCard extends Card{ 
    private int n;
    public PipCard(Suit suit, int n){
        super(suit);
        this.n = n;
    }
    public String toString(){ 
        if( n == 1) return "ACE of SPACE";
        return( suit.toString() + "(" + n +")");
    }
}
